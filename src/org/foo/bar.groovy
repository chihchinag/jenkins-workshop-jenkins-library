package org.foo

  class bar implements Serializable {

    def host1 = "Rico"
    def host2 = "John"
    def topic = "jenkins-workshop"
    def organizer = "devops"
    def date = "2020-06-21"

    def gethost1() {
      return "host1 = ${host1}"
    }

    def gethost2() {
      return "host2 = ${host2}"
    }

    def gettopic() {
      return "topic = ${topic}"
    }
    
    def getorganizer() {
      return "organizer = ${organizer}"
    }
    
    def getdate() {
      return "date = ${date}"
    }
    
    def getVersion(String BUILD_NUMBER, String GIT_COMMIT) {
        return "Build_Number: ${BUILD_NUMBER}, Git_Commit: ${GIT_COMMIT}"
    }

  }